from selenium import webdriver
import smtplib

from selenium.webdriver.remote.command import Command

import names
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC



chrome = "C:\Program Files (x86)\chromedriver.exe"
driver = webdriver.Chrome(chrome)
driver.get("https://classroom.btu.edu.ge/ge/")
email = driver.find_element_by_id("username")
password = driver.find_element_by_id("password")
# ემაილი და პაროლი განთავსებული მაქვს names.py ფაილში. პირდაპირ ეს კოდი არ გაეშვება!!!
email.send_keys(names.email_user)
email.send_keys(Keys.TAB)
password.send_keys(names.password_user)
password.send_keys(Keys.RETURN)
# try except ვიყენებ იმისთვის რომ სანამ კონკრეტულ გვერდს არ გახსნის პროგრამა მანამდე msg ცვლადი გამოიწვევს ერორს
while True:
    try:
        msg = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, "//span[contains(@class,'badge badge-important')]")))
        msg1 = driver.find_element_by_xpath("//span[contains(@class,'badge badge-important')]").text
        if msg1!=0:
            EMAIL_ADDRESS = names.email_user
            EMAIL_PASSWORD = names.password_user
            with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
                smtp.ehlo()
                smtp.starttls()
                smtp.ehlo()
                smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
                smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, "type characters only in ASCII range")
                break
        else:
            driver.implicitly_wait(180)
            driver.refresh()


    except:
        print("მსგავსი ელემენტი ვერ მოიძებნა")

    driver.implicitly_wait(10)
    driver.refresh()
